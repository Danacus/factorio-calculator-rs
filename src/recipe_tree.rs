use crate::recipe_data::{RecipeData, RecipeHandle, ItemHandle, Item};
use std::sync::{Arc, Mutex};
use std::thread;
use std::collections::HashMap;

#[derive(Debug)]
pub struct ItemNode {
    pub id: usize,
    pub item: Item,
    pub recipes: Vec<Arc<RecipeNode>>,
    pub depth: i32,
}

#[derive(Debug)]
pub struct RecipeNode {
    pub id: usize,
    pub recipe: RecipeHandle,
    pub ingredients: Vec<Arc<Mutex<ItemNode>>>,
}

#[derive(Debug)]
pub struct RecipeTree {
    pub root: Arc<Mutex<ItemNode>>,
    pub recipe_data: Arc<RecipeData>,
    pub trims: Arc<HashMap<String, f64>>,
    pub id_counter: Mutex<usize>,
}

struct TreeBuilder {
    shared_stack: Arc<Mutex<Vec<Arc<Mutex<ItemNode>>>>>,
    local_stack: Vec<Arc<Mutex<ItemNode>>>,
    tree: Arc<RecipeTree>,
    max_depth: i32,
}

impl TreeBuilder {
    fn new(
        shared_stack: Arc<Mutex<Vec<Arc<Mutex<ItemNode>>>>>,
        tree: Arc<RecipeTree>,
        max_depth: i32,
    ) -> TreeBuilder {
        TreeBuilder {
            shared_stack,
            local_stack: Vec::new(),
            tree,
            max_depth,
        }
    }

    fn run_thread(mut self) -> thread::JoinHandle<()> {
        thread::spawn(move || {
            let max_depth = self.max_depth;

            while self.local_stack.len() > 0 || self.shared_stack.lock().unwrap().len() > 0 {
                //println!("Found work");

                let new_items = if self.local_stack.len() > 0 {
                    //println!("Found own work");
                    if let Some(item) = self.local_stack.pop().clone() {
                        self.tree.get_item_children(item)
                    } else {
                        continue
                    }
                } else {
                    //println!("Taking new job");
                    let mut stack_lock = self.shared_stack.lock().unwrap();
                    if let Some(item) = stack_lock.pop().clone() {
                        std::mem::drop(stack_lock);

                        self.tree.get_item_children(item)
                    } else {
                        continue
                    }
                };

                for new_item in new_items.iter().filter(|item| item.lock().unwrap().depth < max_depth) {
                    if self.local_stack.len() < 5 {
                        self.local_stack.push(new_item.clone());
                        //println!("Local stack: {}", self.local_stack.len());
                    } else {
                        self.shared_stack.lock().unwrap().push(new_item.clone());
                        //println!("Shared stack: {}", self.shared_stack.lock().unwrap().len());
                    }
                }

            }
        })
    }
}

impl RecipeTree {
    pub fn build(recipe_data: Arc<RecipeData>, target: ItemHandle, max_depth: i32, trims: Arc<HashMap<String, f64>>) -> Arc<RecipeTree> {
        println!("Start tree build");

        let target_node = Arc::new(Mutex::new(ItemNode { 
            item: (*recipe_data.get_item(target).unwrap()).clone(), 
            recipes: Vec::new(), 
            depth: 0, 
            id: 0,
        }));

        let tree = Arc::new(
            RecipeTree {
                root: target_node.clone(),
                recipe_data: recipe_data.clone(),
                trims,
                id_counter: Mutex::new(1),
            }
        );

        let stack_shared = Arc::new(Mutex::new(Vec::new())); 
        stack_shared.lock().unwrap().push(target_node);

        let mut handlers = Vec::new();

        println!("Spawning threads");

        for _ in 0..12 {
            let builder = TreeBuilder::new(
                Arc::clone(&stack_shared), 
                Arc::clone(&tree), 
                max_depth,
            );
            handlers.push(builder.run_thread());
        }

        for handler in handlers {
            handler.join().unwrap();
        }

        println!("Tree is done");

        tree
    }

    fn get_item_children(
        &self, 
        current_target: Arc<Mutex<ItemNode>>,
    ) -> Vec<Arc<Mutex<ItemNode>>> {
        let mut next_nodes = Vec::new();

        let name = current_target.lock().unwrap().item.name.clone();
        let amount = current_target.lock().unwrap().item.amount;
        let depth = current_target.lock().unwrap().depth;

        let recipes = if let Some(vec) = self.recipe_data.get_recipes_for_item(name.clone()) {
            vec.iter().map(|recipe| {
                let result_amount = self.recipe_data.get_item(*self.recipe_data.get_recipe(*recipe).unwrap().results
                    .iter().filter(|res| self.recipe_data.get_item(**res).unwrap().name == name).next().unwrap()).unwrap().amount;

                let recipe_node = Arc::new(RecipeNode { 
                    recipe: recipe.clone(), 
                    ingredients: self.recipe_data.get_recipe(*recipe).unwrap().ingredients.iter().map(|item| {
                        let mut item = (*self.recipe_data.get_item(*item).unwrap()).clone();
                        item.amount *= amount / result_amount;
                        let item_node = Arc::new(Mutex::new(ItemNode { 
                            item, 
                            recipes: Vec::new(), 
                            depth: depth + 1,
                            id: *self.id_counter.lock().unwrap(),
                        }));
                        *self.id_counter.lock().unwrap() += 1;

                        if !self.trims.contains_key(&item_node.lock().unwrap().item.name) {
                            next_nodes.push(item_node.clone());
                        }
                        item_node
                    }).collect(),
                    id: *self.id_counter.lock().unwrap(),
                });
                *self.id_counter.lock().unwrap() += 1;
                recipe_node
            }).collect()
        } else {
            Vec::new()
        };

        current_target.lock().unwrap().recipes = recipes;

        next_nodes
    }
}

