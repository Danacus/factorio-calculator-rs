use crate::recipe_tree::{RecipeTree, RecipeNode, ItemNode};
use crate::recipe_data::{RecipeData, RecipeHandle, ItemHandle, Item};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

pub struct TreeWeights {
    pub recipe_tree: Arc<RecipeTree>,
    pub item_weights: HashMap<String, f64>,
    pub recipe_weights: HashMap<String, f64>,
}

impl TreeWeights {
    pub fn new(recipe_tree: Arc<RecipeTree>) -> TreeWeights {
        TreeWeights {
            recipe_tree,
            item_weights: HashMap::new(),
            recipe_weights: HashMap::new(),
        }
    }
}

pub trait TreeEval {
    fn eval(tree_weights: &mut TreeWeights);
}

pub struct StandardEval;

impl StandardEval {
    pub fn new() -> StandardEval { StandardEval {} }
}

impl TreeEval for StandardEval {
    fn eval(tree_weights: &mut TreeWeights) {
        let tree = tree_weights.recipe_tree.clone();

        println!("Evaluating tree");
        let mut stack = Arc::new(Mutex::new(Vec::new()));
        let mut size = 1;
        stack.lock().unwrap().push(tree.root.clone());
        let mut counter = 20;

        while size > 0 {
            let mut weight: Option<f64> = None;
            let top_node = stack.lock().unwrap().last().unwrap().clone();
            let recipes = top_node.lock().unwrap().recipes.clone();

            println!("{}", top_node.lock().unwrap().item.name);

            if recipes.len() == 0 {
                println!("No recipes found");
                if let Some(item_weight) = tree.trims.get(&top_node.lock().unwrap().item.name) {
                    weight = Some(*item_weight);
                } else {
                    weight = Some(1.0);
                }
            }

            for recipe in &recipes {
                if let Some(w) = tree_weights.recipe_weights.get(&tree.recipe_data.get_recipe(recipe.recipe).unwrap().name.clone()) {
                    if let Some(some_weight) = weight {
                        if *w < some_weight {
                            weight = Some(*w);
                        } 
                    } else {
                        weight = Some(*w);
                    }
                    continue
                }

                println!("Recipe has no weight");

                let mut w = 0.0;
                let mut ok = true;

                for ing in &recipe.clone().ingredients {
                    let amount = ing.lock().unwrap().item.amount;
                    println!("Found Ingredient, amount: {}", amount);

                    match tree_weights.item_weights.get(&ing.lock().unwrap().item.name.clone()) {
                        Some(item_weight) => {
                            println!("Ingredient has weight: {}", item_weight);
                            w += item_weight * amount
                        },
                        None => {
                            println!("Ingredient has no weight, pushing to stack");
                            stack.lock().unwrap().push(ing.clone());
                            size += 1;
                            ok = false;
                            continue
                        }
                    }
                }

                if ok {
                    println!("Assigning weight to recipe: {}", w);
                    tree_weights.recipe_weights.insert(tree.recipe_data.get_recipe(recipe.recipe).unwrap().name.clone(), w);
                    //println!("New weight: {}", recipe.weight.lock().unwrap().unwrap_or_else(|| -1.0));
                }
            }

            if let Some(weight) = weight {
                let top_node = stack.lock().unwrap().pop().unwrap();
                size -= 1;
                tree_weights.item_weights.insert(top_node.lock().unwrap().item.name.clone(), weight);
                /*
                let mut new_recipes = Vec::new();

                for recipe in &top_node.lock().unwrap().recipes {
                    if *tree_weights.recipe_weights.get(&tree.recipe_data.get_recipe(recipe.recipe).unwrap().name.clone()).unwrap_or_else(|| &-1.0) == weight {
                        new_recipes.push(recipe.clone());
                    }
                }

                top_node.lock().unwrap().recipes = new_recipes;*/
            }

            counter -= 1;
        }
    }
}
