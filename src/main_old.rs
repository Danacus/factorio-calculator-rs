use serde_json::{Value};
use std::fs::File;
use std::collections::HashMap;
use std::fmt::Debug;
use std::rc::Rc;
use std::cell::RefCell;
use std::sync::{Arc, Mutex};
use std::thread;
use std::sync::mpsc;

#[derive(Debug)]
struct Item {
    name: String,
    amount: f64,
}

#[derive(Debug)]
struct Recipe {
    name: String,
    ingredients: Vec<Item>,
    results: Vec<Item>,
}

#[derive(Debug)]
struct ItemNode {
    item: Arc<Mutex<Item>>,
    recipes: Vec<Arc<RecipeNode>>,
    depth: i32,
}

#[derive(Debug)]
struct RecipeNode {
    recipe: Arc<Mutex<Recipe>>,
    ingredients: Vec<Arc<Mutex<ItemNode>>>,
}


type RecipeMap = HashMap<&'static str, Vec<Arc<Mutex<Recipe>>>>;

fn main() {
    println!("Hello, world!");

    let data: Value = read_data().unwrap_or_else(|err| panic!(err));
    let recipes: HashMap<String, Recipe> = load_data(data).unwrap_or_else(|err| panic!(err));
    let recipe_map = Arc::new(Mutex::new(create_recipe_map(recipes)));

    let target = Item { name: "circuit-red-loaded".to_string(), amount: 1.0 };
    let tree = create_tree(Arc::clone(&recipe_map), target, 3);
    //println!("{:#?}", recipes["circuit-red-loaded"]);
    //println!("{:#?}", recipes);
    //println!("{:#?}", recipe_map["circuit-red-loaded"]);
    //println!("{:#?}", recipe_map["iron-plate"]);
    println!("{:#?}", &tree);
}

fn create_tree(recipe_map: Arc<Mutex<RecipeMap>>, target: Item, depth: i32) -> Arc<Mutex<ItemNode>> {
    let stack = Arc::new(Mutex::new(Vec::new())); 
    let target_node = Arc::new(Mutex::new(ItemNode { item: Arc::new(Mutex::new(target)), recipes: Vec::new(), depth: 0, }));
    stack.lock().unwrap().push(Arc::clone(&target_node));

    let mut handlers = Vec::new();

    /*
    for _ in 0..5 {
        let stack = Arc::clone(&stack);
        let recipe_map = Arc::clone(&recipe_map);

        handlers.push(thread::spawn(move || {
            while stack.lock().unwrap().len() > 0 {
                let item = stack.lock().unwrap().pop().unwrap();  

                let new_items = get_item_children(recipe_map, item);

                for new_item in new_items {
                    if new_item.lock().unwrap().depth < depth {
                        stack.lock().unwrap().push(new_item)
                    }
                }
            }
        }));
    }
    */

    let (tx, rx) = mpsc::channel();

    while stack.lock().unwrap().len() > 0 {
        let item = stack.lock().unwrap().pop().unwrap();  

        //let new_items = get_item_children(recipe_map, item);
        let tx = mpsc::Sender::clone(&tx); 
        let recipe_map = Arc::clone(&recipe_map);

        handlers.push(thread::spawn(move || {
            let new_items = get_item_children(recipe_map, item);
            for new_item in new_items {
                if new_item.lock().unwrap().depth < depth {
                    tx.send(new_item);
                }
            }
        }));

        for recieved in &rx {
            stack.lock().unwrap().push(recieved)
        }
    }

    for handler in handlers {
        handler.join();
    }

    Arc::clone(&target_node)
}

fn get_item_children<'a>(recipe_map: Arc<Mutex<RecipeMap>>, current_target: Arc<Mutex<ItemNode>>) 
    -> Vec<Arc<Mutex<ItemNode>>> 
{
    let mut next_nodes = Vec::new();

    let name = current_target.lock().unwrap().item.lock().unwrap().name.clone();
    println!("{}", name);
    let depth = current_target.lock().unwrap().depth;

    if let Some(entry) = recipe_map.lock().unwrap().get(&name[..]) {
        let recipes = entry.iter().map(|recipe| {
            Arc::new(RecipeNode { 
                recipe: recipe.clone(), 
                ingredients: recipe.lock().unwrap().ingredients.iter().map(|item| {
                    let item = Arc::new(Mutex::new(ItemNode { 
                        item: Arc::new(Mutex::new(*item)), 
                        recipes: Vec::new(), 
                        depth: depth + 1,
                    }));
                    next_nodes.push(item.clone());
                    item
                }).collect(),
            })
        }).collect();

        current_target.lock().unwrap().recipes = recipes;
    }

    next_nodes
}

fn create_recipe_map(recipes: HashMap<String, Recipe>) -> RecipeMap {
    let mut map: HashMap<_, Vec<_>> = HashMap::new();

    for (_, recipe) in recipes.iter() {
        for result in &recipe.results {
            if let Some(vec) = map.get_mut(&result.name[..]) {
                vec.push(Arc::new(Mutex::new(recipe))); 
            } else {
                map.insert(&result.name[..], vec![Arc::new(Mutex::new(recipe))]);
            }
        }
    }

    map
}

fn read_data() -> serde_json::Result<Value> {
    let f = File::open("data.json").unwrap();
    return serde_json::from_reader(f);
}

fn load_data(data: Value) -> Result<HashMap<String, Recipe>, &'static str> {
    if let Value::Object(map) = data {
        if let Value::Object(recipes) = &map["recipes"] {
            let mut recipe_map = HashMap::new();

            for (name, recipe) in recipes {
                let recipe = load_recipe(name, recipe)?;
                recipe_map.insert(name.clone(), recipe);
            }

            Ok(recipe_map)
        } else {
            Err("Recipes is not an Object")
        }
    } else {
        Err("Parsing JSON data failed")
    }
}

fn load_recipe(name: &String, recipe_obj: &Value) -> Result<Recipe, &'static str> {
    if let Value::Object(recipe_map) = recipe_obj {
        if let (Value::Array(ing), Value::Array(res)) = (&recipe_map["ingredients"], &recipe_map["results"]) {
            let ingredients = ing.iter().map(|item| load_item(item).unwrap_or_else(|e| panic!(e))).collect();
            let results = res.iter().map(|item| load_item(item).unwrap_or_else(|e| panic!(e))).collect();
            
            Ok(Recipe { name: name.clone(), ingredients, results, })
        } else {
            Err("ingredients or results is not an array")
        }
    } else {
        Err("Failed to parse recipe")
    }
}

fn load_item(item_obj: &Value) -> Result<Item, &'static str> {
    if let Value::Object(item_map) = item_obj {
        if let (Value::String(name), Value::Number(amount)) = (&item_map["name"], &item_map["amount"]) {
            Ok(Item { name: name.clone(), amount: amount.as_f64().unwrap(), })
        } else {
            Err("name is not a string or amount is not a number")
        }
    } else {
        Err("Item is not an object")
    }
}
