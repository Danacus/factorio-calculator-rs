mod recipe_data;
mod recipe_tree;
mod load_data;
mod dot_graph;
mod tree_eval;

use std::fs::File;
use std::io::Write;
use std::process::Command;
use std::sync::{Arc};
use std::collections::HashMap;

use crate::recipe_data::{ItemHandle, Item};
use crate::recipe_tree::{RecipeTree};
use crate::dot_graph::{DotGraph};
use crate::load_data::{load_recipe_data};
use crate::tree_eval::{TreeWeights, TreeEval, StandardEval};


fn main() {
    test_angels();
}

fn test_angels() {
    let mut recipe_data = load_recipe_data("data-angels.json").unwrap_or_else(|err| panic!(err));

    let mut dot_graph = DotGraph {
        dot_nodes: Vec::new(),
        dot_edges: Vec::new(),
    };

    let items = vec![
        "circuit-green-loaded",
    ];

    let mut trims = HashMap::new();
    trims.insert("iron-plate".to_string(), 10.0);
    trims.insert("copper-plate".to_string(), 10.0);
    trims.insert("angels-solder".to_string(), 10.0);
    trims.insert("angels-wire-coil-copper".to_string(), 5.0);
    trims.insert("angels-wire-tin".to_string(), 10.0);
    let trims = Arc::new(trims);

    let targets: Vec<ItemHandle> = items.iter().map(|item| 
        recipe_data.add_item(Item { name: item.to_string(), amount: 1.0, })
    ).collect();

    let recipe_data = Arc::new(recipe_data);

    for target in targets {
        let recipe_data = recipe_data.clone();
        let tree = RecipeTree::build(recipe_data, target, 3, trims.clone());
        let mut tree_weights = TreeWeights::new(tree.clone());
        StandardEval::eval(&mut tree_weights);
        RecipeTree::dot_build_graph(&mut dot_graph, tree, &tree_weights);
    }

    println!("here");

    {
        let mut f = File::create("graph.dot").unwrap();
        dot_graph.dot_render(&mut f);
    }

    let s = std::fs::read_to_string("graph.dot").unwrap();
    let s = format!("strict {}", s);
    std::fs::write("graph.dot", s).unwrap();

    let output = Command::new("dot")
            .arg("-Tpng")
            .arg("graph.dot")
            .output()
            .expect("failed to execute process");
    
    {
        let mut f = File::create("graph.png").unwrap();
        f.write(&output.stdout).unwrap();
    }


    Command::new("imv")
            .arg("graph.png")
            .output()
            .expect("failed to execute process");
}

fn test_vanilla() {
    let mut recipe_data = load_recipe_data("data-vanilla.json").unwrap_or_else(|err| panic!(err));

    let mut dot_graph = DotGraph {
        dot_nodes: Vec::new(),
        dot_edges: Vec::new(),
    };

    let items = vec![
        //"automation-science-pack",
        //"logistic-science-pack",
        //"military-science-pack", 
        //"chemical-science-pack",
        //"production-science-pack",
        "utility-science-pack",
    ];

    let mut trims = HashMap::new();
    trims.insert("iron-plate".to_string(), 10.0);
    trims.insert("copper-plate".to_string(), 10.0);
    trims.insert("coal".to_string(), 10.0);
    trims.insert("steel-plate".to_string(), 20.0);
    trims.insert("crude-oil".to_string(), 1.0);
    trims.insert("water".to_string(), 0.0);
    //trims.insert("electronic-circuit".to_string());
    //trims.insert("advanced-circuit".to_string());
    //trims.insert("processing-unit".to_string());
    let trims = Arc::new(trims);

    let targets: Vec<ItemHandle> = items.iter().map(|item| 
        recipe_data.add_item(Item { name: item.to_string(), amount: 1.0, })
    ).collect();

    let recipe_data = Arc::new(recipe_data);

    for target in targets {
        let recipe_data = recipe_data.clone();
        let tree = RecipeTree::build(recipe_data, target, 5, trims.clone());
        let mut tree_weights = TreeWeights::new(tree.clone());
        StandardEval::eval(&mut tree_weights);
        RecipeTree::dot_build_graph(&mut dot_graph, tree.clone(), &tree_weights);
    }

    println!("here");

    {
        let mut f = File::create("graph.dot").unwrap();
        dot_graph.dot_render(&mut f);
    }

    let s = std::fs::read_to_string("graph.dot").unwrap();
    let s = format!("strict {}", s);
    std::fs::write("graph.dot", s).unwrap();

    let output = Command::new("dot")
            .arg("-Tpng")
            .arg("graph.dot")
            .output()
            .expect("failed to execute process");
    
    {
        let mut f = File::create("graph.png").unwrap();
        f.write(&output.stdout).unwrap();
    }


    Command::new("imv")
            .arg("graph.png")
            .output()
            .expect("failed to execute process");
}

fn test_benchmark() {
    let mut recipe_data = load_recipe_data("data-angels.json").unwrap_or_else(|err| panic!(err));

    let mut dot_graph = DotGraph {
        dot_nodes: Vec::new(),
        dot_edges: Vec::new(),
    };

    let target = recipe_data.add_item(Item {
        name: "circuit-red-loaded".to_string(),
        amount: 1.0,
    });

    let mut tree = RecipeTree::build(Arc::new(recipe_data), target, 10, Arc::new(HashMap::new()));
    /*
    RecipeTree::dot_build_graph(&mut dot_graph, tree);

    {
        let mut f = File::create("graph.dot").unwrap();
        dot_graph.dot_render(&mut f);
    }

    let s = std::fs::read_to_string("graph.dot").unwrap();
    let s = format!("strict {}", s);
    std::fs::write("graph.dot", s).unwrap();

    let output = Command::new("dot")
            .arg("-Tpng")
            .arg("graph.dot")
            .output()
            .expect("failed to execute process");
    
    {
        let mut f = File::create("graph.png").unwrap();
        f.write(&output.stdout).unwrap();
    }


    Command::new("imv")
            .arg("graph.png")
            .output()
            .expect("failed to execute process");

    */
}
