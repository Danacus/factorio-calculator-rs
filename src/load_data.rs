use serde_json::{Value};
use std::fs::File;
use crate::recipe_data::{RecipeData, ItemHandle, Recipe, Item};

pub fn load_recipe_data(filename: &'static str) -> Result<RecipeData, &'static str> {
    let data: Value = if let Ok(data) = read_data(filename) { 
        data 
    } else { 
        return Err("Failed to read data") 
    };
    let mut recipe_data = load_data(data)?;
    recipe_data.build_lookup_table();
    Ok(recipe_data)
}

fn read_data(filename: &'static str) -> serde_json::Result<Value> {
    let f = File::open(filename).unwrap();
    return serde_json::from_reader(f);
}

fn load_data(data: Value) -> Result<RecipeData, &'static str> {
    if let Value::Object(map) = data {
        if let Value::Object(recipes) = &map["recipes"] {
            let mut recipe_data = RecipeData::new();

            for (name, recipe) in recipes {
                let recipe = load_recipe(&mut recipe_data, name, recipe)?;
                recipe_data.add_recipe(recipe);
            }

            Ok(recipe_data)
        } else {
            Err("Recipes is not an Object")
        }
    } else {
        Err("Parsing JSON data failed")
    }
}

fn load_recipe(mut recipe_data: &mut RecipeData, name: &String, recipe_obj: &Value) -> Result<Recipe, &'static str> {
    if let Value::Object(recipe_map) = recipe_obj {
        if let (Value::Array(ing), Value::Array(res)) = (&recipe_map["ingredients"], &recipe_map["results"]) {
            let ingredients = load_items(&mut recipe_data, ing)?;
            let results = load_items(&mut recipe_data, res)?;
            
            Ok(Recipe { name: name.clone(), ingredients, results, })
        } else {
            Err("Ingredients or results is not an array")
        }
    } else {
        Err("Failed to parse recipe")
    }
}

fn load_items(mut recipe_data: &mut RecipeData, item_objs: &Vec<Value>) -> Result<Vec<ItemHandle>, &'static str> {
    let mut vec = Vec::new();

    for obj in item_objs {
        vec.push(load_item(&mut recipe_data, obj)?); 
    }

    Ok(vec)
}

fn load_item(recipe_data: &mut RecipeData, item_obj: &Value) -> Result<ItemHandle, &'static str> {
    if let Value::Object(item_map) = item_obj {
        if let (Value::String(name), Value::Number(amount)) = (&item_map["name"], &item_map["amount"]) {
            Ok(recipe_data.add_item(Item { name: name.clone(), amount: amount.as_f64().unwrap(), }))
        } else {
            Err("Failed to parse item: Name is not a string, or amount is not a number")
        }
    } else {
        Err("Failed to parse item: Item is not an object")
    }
}
