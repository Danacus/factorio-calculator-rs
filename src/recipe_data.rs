use std::sync::{Arc};
use std::collections::HashMap;

pub type RecipeHandle = usize;
pub type ItemHandle = usize;

#[derive(Debug, Clone)]
pub struct Item {
    pub name: String,
    pub amount: f64,
}

#[derive(Debug)]
pub struct Recipe {
    pub name: String,
    pub ingredients: Vec<ItemHandle>,
    pub results: Vec<ItemHandle>,
}

#[derive(Debug)]
pub struct RecipeData {
    recipes: Vec<Arc<Recipe>>,
    items: Vec<Arc<Item>>,
    recipe_map: HashMap<String, RecipeHandle>,
    item_lookup_table: HashMap<String, Vec<RecipeHandle>>,
}

impl RecipeData {
    pub fn new() -> RecipeData {
        RecipeData {
            recipes: Vec::new(),
            items: Vec::new(),
            recipe_map: HashMap::new(),
            item_lookup_table: HashMap::new(),
        }
    }

    pub fn add_recipe(&mut self, recipe: Recipe) -> RecipeHandle {
        let handle = self.recipes.len();
        self.recipe_map.insert(recipe.name.clone(), handle);
        self.recipes.push(Arc::new(recipe)); 
        handle
    }

    pub fn add_item(&mut self, item: Item) -> ItemHandle {
        let handle = self.items.len();
        self.items.push(Arc::new(item)); 
        handle
    }

    pub fn get_recipe(&self, recipe_handle: RecipeHandle) -> Option<Arc<Recipe>> {
        match self.recipes.get(recipe_handle) {
            Some(recipe) => Some(recipe.clone()),
            None => None
        }
    }

    pub fn get_recipes_for_item(&self, name: String) -> Option<&Vec<RecipeHandle>> {
        self.item_lookup_table.get(&name)
    }

    pub fn get_item(&self, item_handle: ItemHandle) -> Option<Arc<Item>> {
        match self.items.get(item_handle) {
            Some(item) => Some(item.clone()),
            None => None,
        }
    }

    pub fn build_lookup_table(&mut self) {
        for (handle, recipe) in self.recipes.iter().enumerate() {
            for result in &recipe.results {
                let name = self.get_item(*result).unwrap().name.clone();

                if let Some(vec) = self.item_lookup_table.get_mut(&name) {
                    vec.push(handle); 
                } else {
                    self.item_lookup_table.insert(name, vec![handle]);
                }
            }
        }
    }
}
