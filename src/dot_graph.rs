use crate::recipe_tree::{RecipeTree};
use crate::tree_eval::{TreeWeights};
use std::sync::{Arc};
use std::borrow::Cow;
use std::io::Write;

impl RecipeTree {
    pub fn dot_build_graph(dot_graph: &mut DotGraph, tree: Arc<RecipeTree>, tree_weights: &TreeWeights) {
        let mut stack = Vec::new();
        stack.push(tree.root.clone());
        
        while let Some(item_node) = stack.pop() {
            let item_lock = item_node.lock().unwrap();
            let item_dot = (format!("{} ({:.2}) {}", item_lock.item.name.clone(), item_lock.item.amount, item_lock.id), "item");
            dot_graph.dot_nodes.push(item_dot.clone());

            for recipe_node in &item_lock.recipes {
                let recipe_dot = (format!("{} ({}) {}", 
                        tree.recipe_data.get_recipe(recipe_node.clone().recipe).unwrap().name.clone(), 
                        tree_weights.recipe_weights.get(
                            &tree.recipe_data.get_recipe(recipe_node.recipe).unwrap().name.clone()
                        ).unwrap_or_else(|| &-1.0),
                        recipe_node.id,
                ), "recipe");
                dot_graph.dot_nodes.push(recipe_dot.clone());
                dot_graph.dot_edges.push((
                    item_dot.clone(),
                    recipe_dot.clone(),
                ));
                
                for new_item_node in &recipe_node.ingredients {
                    stack.push(new_item_node.clone());
                    let item_lock = new_item_node.lock().unwrap();
                    let item_dot = (format!("{} ({:.2}) {}", item_lock.item.name.clone(), item_lock.item.amount, item_lock.id), "item");
                    dot_graph.dot_edges.push((
                        recipe_dot.clone(),
                        item_dot,
                    ));
                }
            }
        }
    }
}

pub struct DotGraph {
    pub dot_nodes: Vec<Nd>,
    pub dot_edges: Vec<Ed>,
}

impl DotGraph {
    pub fn dot_render<W: Write>(&self, output: &mut W) {
        dot::render(self, output).unwrap()
    }
}

impl<'a> dot::Labeller<'a, Nd, Ed> for DotGraph {
    fn graph_id(&self) -> dot::Id { 
        dot::Id::new("RecipeTree").unwrap()
    }
    fn node_id(&self, n: &Nd) -> dot::Id {
        let text: String = n.0.chars()
            .map(|x| match x { 
                '-' => '_', 
                ' ' => '_', 
                '(' => '_',
                ')' => '_',
                '.' => '_',
                _ => x
            }).collect();
        dot::Id::new(format!("{}_{}", n.1, text)).unwrap()
    }
    fn node_label(&self, n: &Nd) -> dot::LabelText {
        let label = match n.1 {
            "item" => format!("I: {}", n.0.clone()),
            "recipe" => format!("R: {}", n.0.clone()),
            _ => "".to_string(),
        };
        dot::LabelText::LabelStr(Cow::from(label))
    }
    fn edge_label(&self, _: &Ed) -> dot::LabelText {
        dot::LabelText::LabelStr(Cow::from(""))
    }
}

impl<'a> dot::GraphWalk<'a, Nd, Ed> for DotGraph {
    fn nodes(&self) -> dot::Nodes<Nd> {
        Cow::from(&self.dot_nodes)
    }
    fn edges(&self) -> dot::Edges<Ed> {
        Cow::from(&self.dot_edges)
    }
    fn source(&self, e: &Ed) -> Nd { e.0.clone() }
    fn target(&self, e: &Ed) -> Nd { e.1.clone() }
}

type Nd = (String, &'static str);
type Ed = (Nd, Nd);
